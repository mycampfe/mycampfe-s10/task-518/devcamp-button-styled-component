import {SuperButton, ExtendedButton} from "./components/ExtendedButton";
import { PropsButton, PrimaryButton } from "./components/PropsButton";
import StyleButton from "./components/StyledButton";

function App() {
  return (
    <div>
        <div>
          <StyleButton>I am a styled button!</StyleButton>
        </div>
        <div style={{display:"none"}}>
          <PropsButton backColor="#ffc3c3" fontColor="#bf576b">Button 1</PropsButton>
          <PropsButton backColor="#ffdaca" fontColor="#e07e64">Button 2</PropsButton>
          <PropsButton backColor="#fff4c7" fontColor="#cfb76f">Button 3</PropsButton>
          <PropsButton>Button 3</PropsButton>
        </div>
        <div style={{display:"none"}}>
          <PrimaryButton primary>Primary Button</PrimaryButton>
          <PrimaryButton>Other Button 1</PrimaryButton>
          <PrimaryButton>Other Button 2</PrimaryButton>
        </div>
        <div style={{display:"none"}}>
          <SuperButton>Super Button</SuperButton>
          <ExtendedButton bgColor="#ff1993" ftColor="#ffe6f8">Hello 1!😍</ExtendedButton>
          <ExtendedButton bgColor="#ff69b4" ftColor="#ffe3f3pi">Hello 2!</ExtendedButton>
          <ExtendedButton>Hello 3!</ExtendedButton>
        </div>

    </div>
  );
}

export default App;
