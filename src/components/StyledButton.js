import styled from "styled-components";

/*
const StyleButton = styled.button ({
    backgroundColor: "rgb(123, 76, 216)",
    color: "#ffffff",
    border: "none",
    padding: "10px 20px",
    borderRadius: "5px",
    marginTop: "10px",
    fontSize: "14px"
})
*/
/*
const StyleButton = styled['button']`
background-color: rgb(123, 76, 216);
color: #ffffff;
border: none;
padding: 10px 20px;
border-radius: 5px;
margin-top: 10px;
font-size: 14px;
`
*/

const StyleButton = styled.button`
background-color: rgb(123, 76, 216);
color: #ffffff;
border: none;
padding: 10px 20px;
border-radius: 5px;
margin: 10px;
font-size: 14px;
`

export default StyleButton